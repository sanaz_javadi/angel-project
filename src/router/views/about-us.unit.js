import AboutUs from './about-us'

describe('@views/about-us', () => {
  it('is a valid view', () => {
    expect(AboutUs).toBeAViewComponent()
  })
})
