import Gallery from './gallery'

describe('@views/gallery', () => {
  it('is a valid view', () => {
    expect(Gallery).toBeAViewComponent()
  })
})
