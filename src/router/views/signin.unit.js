import Signin from './signin'

describe('@views/signin', () => {
  it('is a valid view', () => {
    expect(Signin).toBeAViewComponent()
  })
})
