import MainFooter from './main-footer'

describe('@components/main-footer', () => {
  it('exports a valid component', () => {
    expect(MainFooter).toBeAComponent()
  })
})
