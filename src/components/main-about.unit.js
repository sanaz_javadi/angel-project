import MainAbout from './main-about'

describe('@components/main-about', () => {
  it('exports a valid component', () => {
    expect(MainAbout).toBeAComponent()
  })
})
