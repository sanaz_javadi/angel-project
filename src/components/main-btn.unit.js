import MainBtn from './main-btn'

describe('@components/main-btn', () => {
  it('exports a valid component', () => {
    expect(MainBtn).toBeAComponent()
  })
})
