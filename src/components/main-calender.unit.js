import MainCalender from './main-calender'

describe('@components/main-calender', () => {
  it('exports a valid component', () => {
    expect(MainCalender).toBeAComponent()
  })
})
