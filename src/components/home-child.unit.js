import HomeChild from './home-child'

describe('@components/home-child', () => {
  it('exports a valid component', () => {
    expect(HomeChild).toBeAComponent()
  })
})
