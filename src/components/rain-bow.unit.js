import RainBow from './rain-bow'

describe('@components/rain-bow', () => {
  it('exports a valid component', () => {
    expect(RainBow).toBeAComponent()
  })
})
